package com.rizal.binarmove.data.repository


import com.rizal.binarmove.data.model.Genres
import com.rizal.binarmove.data.model.MovieDetailModel
import com.rizal.binarmove.data.model.MovieResult
import com.rizal.binarmove.data.model.MovieReview
import com.rizal.binarmove.data.model.MovieTrailerModel
import com.rizal.binarmove.utils.network.Results

interface Repository {
    suspend fun requestPopularMovie(query: HashMap<String, Any>): Results<MovieResult>
    suspend fun requestMovieDetails(movieId: Int): Results<MovieDetailModel>
    suspend fun requestMovieTrailer(movieId: Int): Results<MovieTrailerModel>
    suspend fun requestGenres(lang: String = "en"): Results<Genres>
    suspend fun requestMovieReviews(movieId: Int, query: HashMap<String, Any>): Results<MovieReview>
}